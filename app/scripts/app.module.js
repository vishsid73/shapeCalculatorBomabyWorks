'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);
