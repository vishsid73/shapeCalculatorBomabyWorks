'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
