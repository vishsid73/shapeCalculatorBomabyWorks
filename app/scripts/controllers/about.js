'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
