'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = {};
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = {};
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = {};
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

'use strict';

/**
 * @ngdoc overview
 * @name bomabyWorksApp
 * @description
 * # bomabyWorksApp
 *
 * Main module of the application.
 */
angular
  .module('bomabyWorksApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the bomabyWorksApp
 */

angular.module('bomabyWorksApp').controller('CalculatorCtrl', CalculatorCtrl);
CalculatorCtrl.$inject = ['$q', '$route', '$rootScope', '$scope'];

function CalculatorCtrl($q, $route, $rootScope, $scope) {
  var calc = this;
  calc.step = 1;
  calc.nextStep = calc.step + 1;

  calc.cancel = function () {
    calc.shape = null;
    calc.dimentions = null;
    calc.area = null;
    calc.step = 1;
    calc.nextStep = 2;
  };

  calc.moveToNextStep = function (step) {
    if (step == 3) {
      calc.cancel();
    }
    else if (step == 2) {
      calc.calculateArea();
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
    else {
      calc.step = step + 1;
      calc.nextStep = calc.step + 1;
    }
  };

  calc.calculateArea = function () {
    var pi = 3.14;
    if (calc.shape == 'square') {
      calc.area = parseFloat(parseFloat(calc.dimentions.side) * parseFloat(calc.dimentions.side));
    }
    if (calc.shape == 'circle') {
      calc.area = parseFloat((parseFloat(calc.dimentions.diameter) / 2) * pi);
    }
    if (calc.shape == 'rectangle') {
      calc.area = parseFloat(parseFloat(calc.dimentions.length) * parseFloat(calc.dimentions.breadth));
    }
    if (calc.shape == 'ellipse') {
      calc.area = parseFloat(parseFloat(calc.dimentions.radius1) * parseFloat(calc.dimentions.radius2) * pi);
    }
    calc.area = Number(Math.round(calc.area + 'e2') + 'e-2');
  };

  calc.checkDisabled = function(step){
    if(step == 1){
      if(calc.shape){
        return false;
      }
      else{
        return true;
      }
    }

    if(step == 2){
      if(calc.dimentions){
        return false;
      }
      else{
        return true;
      }
    }
  };

};

'use strict';

/**
 * @ngdoc function
 * @name bomabyWorksApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bomabyWorksApp
 */
angular.module('bomabyWorksApp')
  .controller('MainCtrl', function () {

  });
