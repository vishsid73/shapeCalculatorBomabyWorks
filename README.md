# Web application - Shape Calculator - bomaby-works

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development

 •	Install npm nd bower

 •	npm install

 •	bower install

 •	Run `grunt` for building and `grunt serve` for preview.

### Summary:
	
	•	Create a web application that calculates shape areas using PHP 5.
	
	•	Apply a simple design using CSS and HTML. Please see the attatched wire frame design (shapecalculator.pdf) for functionality and page layout.

### What to do:

	•	Create a web application.

	•	First web page should show a radio button list of shapes (circle, square, rectangle, elipse).

	•	When you click on a shape, you are asked to enter dimensions.

	◦	For circle, only diameter is required

	◦	For elipse, height and width is required.

	◦	For square, width is is required.

	◦	For rectangle, width and height is required.

	•	After entering the dimensions the area of the shape will be calculated.



### Specification to shapecalculation.pdf:

Image 1:
Display header with text (images/header_bg.gif). To the left is a short informative text of how to use the application, and som dummy text. In the right there is a radio button list of the Shapes avaliable as links. User should select one and click next.

Image 2:
Step 2 includes a form with labels and input filed required for calculating the selected shapes Area. Be aware that different shapes require differerent input fields. When the user clicks Go to step 3 this enables step 3 with the calculated value of the are. 

Image 3:
The result is a dynamic text based on what the user have selected in the previous steps and the calculated area. The submit button takes the user to step 1 again.
